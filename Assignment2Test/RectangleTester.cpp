//Bring in unit testing code and tell it to build a main function
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
//This pragma supresses a bunch of warnings QTCreator produces (and should not)
#pragma clang diagnostic ignored "-Woverloaded-shift-op-parentheses"
#define NOGDI
#include "doctest.h"

//Use Approx from doctest without saying doctest::Approx
using doctest::Approx;


//Include your .h files
#include "Rectangle.h"
#include "Point.h"
TEST_CASE( "Regulr Rectangle Constructor 1" ) {
    Point p1(1,5);
    myRectangle r1(p1,5,5);

   //Check that x is 1. CHECK means keep running this TEST_CASE even if failed.
   Point temp = r1.getUpperLeftVertex();
   REQUIRE( temp.isSameAs(p1) == true );


   CHECK( r1.getHeight() == Approx(5) );
   CHECK( r1.getWidth() == Approx(5) );

    CHECK(r1.getPerimeter()==Approx(20));

}


TEST_CASE( "Regular Rectangle Constructor 2" ) {
    Point p3(1,5);
    Point p4(2,10);
    myRectangle r2(p3,p4);

    Point Upper=r2.getUpperLeftVertex();




    //Value of X should be to the Left so it should be Less then or equal to depending on the point
    REQUIRE(Upper.getX()<=p3.getX());
    REQUIRE(Upper.getX()<=p4.getX());

    //Value of Y should be to the Up so it should equal to or greater  depending on the point
    REQUIRE(Upper.getY()>=p3.getY());
    REQUIRE(Upper.getY()>=p4.getY());

    //Check to make sure Height and Width are okay
   CHECK(r2.getHeight()==Approx(5));
    CHECK(r2.getWidth()==Approx(1));

    CHECK(r2.getPerimeter()==Approx(12));




}

////Include your .h files
//#include "Rectangle.h"
//#include "Point.h"
TEST_CASE( "Negative Rectangle 1" ) {
    Point p1;
    myRectangle r3(p1,5,5);


   Point center=r3.getCenter();

   CHECK(center.getX()==Approx(2.5));
   CHECK(center.getY()==Approx(-2.5));

   CHECK(r3.getPerimeter()==Approx(20));


}

TEST_CASE( "Negative Rectangle 2") {
    Point p3(0,0);
    Point p4(-1,-5);
    myRectangle r2(p3,p4);


    Point Upper=r2.getUpperLeftVertex();
    Point Center=r2.getCenter();




    //Value of X should be to the Left so it should be Less then or equal to depending on the point
    REQUIRE(Upper.getX()<=p3.getX());
    REQUIRE(Upper.getX()<=p4.getX());

    //Value of Y should be to the Up so it should equal to or greater  depending on the point
   REQUIRE(Upper.getY()>=p3.getY());
   REQUIRE(Upper.getY()>=p4.getY());

   //Check if Values are right
   CHECK(Upper.getX()==Approx(-1));
   CHECK(Upper.getY()==Approx(0));

    //Check to make sure Height and Width are okay
   CHECK(r2.getHeight()==Approx(5));
    CHECK(r2.getWidth()==Approx(1));

    //Perimeter

    CHECK(r2.getPerimeter()==Approx(12));

    //Center
    CHECK(Center.getX()==Approx(-.5));
    CHECK(Center.getY()==Approx(-2.5));







}

TEST_CASE( "Translate") {
Point p5(5,5);
myRectangle r5(p5,2,2);
Point Upper = r5.getUpperLeftVertex();
Point Center = r5.getCenter();
//Check Before Move
REQUIRE(r5.getWidth()==Approx(2));
REQUIRE(r5.getHeight()==Approx(2));

CHECK(Upper.getX()==Approx(5));
CHECK(Upper.getY()==Approx(5));

CHECK(Center.getX()==Approx(6));
CHECK(Center.getY()==Approx(4));

//Redo After Moving
r5.translate(2,2);

REQUIRE(r5.getWidth()==Approx(2));
REQUIRE(r5.getHeight()==Approx(2));

Upper = r5.getUpperLeftVertex();
Center = r5.getCenter();



CHECK(Upper.getX()==Approx(7));
CHECK(Upper.getY()==Approx(7));


CHECK(Center.getX()==Approx(8));
CHECK(Center.getY()==Approx(6));

}

TEST_CASE( "Check if Point is contained" ) {
    Point p1(0,10);
    myRectangle r1(p1,8,8);
    Point p2(4,6);
    Point p3(12,14);
    Point p4(0,10);

   CHECK(r1.contains(p2)==true);
   CHECK(r1.contains(p3)==false);
   CHECK(r1.contains(p4)==true);
}

TEST_CASE( "Check Area" ) {
    Point p1(0,10);
    myRectangle r1(p1,8,8);

   CHECK(r1.getArea()==Approx(64));
}
