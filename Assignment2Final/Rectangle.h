#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "Point.h"
class myRectangle
{
public:
myRectangle(Point p1,double recheight, double recwidth);
myRectangle(Point p1,Point p2);
Point getUpperLeftVertex();
double  getHeight();
double  getWidth();
double getPerimeter();
double getArea();
Point getCenter();
void translate(double x,double y);
bool contains(Point p);


private:
    double height;
    double width;
    Point UpperLeftVertex;
    Point LowerRightVertex;
};

#endif // RECTANGLE_H
